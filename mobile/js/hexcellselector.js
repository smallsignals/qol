/*
* Copyright 2007 JORGE ARTIEDA
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/ 

var HexCellSel = function () {

  this.rows = 5;
  this.cols = 9;
  this.scale = 0.1;
  this.onclick = function (e){};
  this.s = Snap("#svg");
  this.draw();
  //this.drawTable();
  this.selection = [];
};
HexCellSel.prototype.rainbow = function (n){
  n =( n / (this.rows* this.cols* 0.9));
  return 'hsl(' + n + ',100%,50%)';
}

HexCellSel.prototype.draw = function(){
  console.log("I am drawing!");
  for (var i = 0; i < this.cols; i ++){
    for (var j = 0; j < this.rows; j++){
      var p =  this.drawHex();
      var t = new Snap.Matrix();
      t.scale(this.scale);
      var off = 0;
      if (i %  2 == 0)
        off = 130;
      t.translate(i*260 , j*260+off);
      p.transform(t);
      p.attr("fill", this.rainbow(i*j));
      p.i = i;
      p.j = j;
    }
  }
  //<polygon class="hex" points="300,150, 225,280, 75,280, 0,150, 75,20, 225,20"></polygon>
};

HexCellSel.prototype.drawPoint = function(i,  j){
  var off = 0;
  if (i %  2 == 0)
    off = 130;
  var xc = i*260+140;
  var yc = j*260+140+off;
  console.log("drawing ("+xc*this.scale+", "+yc*this.scale+")");
  var p = this.s.circle(xc*this.scale, yc*this.scale, 5);
  var h = this;
  p.i = i; p.j = j;
  p.click(function(){
    var m = {'i': p.i, 'j' : p.j};
    h.onclick(m);
  });
/*  for (var i = 0; i< this.hot.data.length;i++){
    var x = this.hot.data[i][1]
    this.s.circle(x,y,3)
  }*/
}
HexCellSel.prototype.drawHex = function(){
  console.log("I am drawing!");

  var p = this.s.polygon([300,150, 225,280, 75,280, 0,150, 75,20, 225,20]).attr({
    fill: "#bada55",
    stroke: "#000",
    strokeWidth: 20
  });
  var t = new Snap.Matrix();
  t.scale(0.1);
  var h = this;
  p.transform(t);
  p.hover(function (){
    p.attr({stroke:"#f00"})
  },function (f){
    p.attr({stroke:"#000"})
  });
  p.click(function(){
    var m = {'i': p.i, 'j' : p.j};
    h.onclick(m);
  });
  return p;
};
/*
HexCellSel.prototype.drawTable=function(){
this.data = [
    ["", "i", "j", "lat", "lon" ],
    ["2008", 10, 11, 12, 13],
    ["2009", 20, 11, 14, 13],
    ["2010", 30, 15, 12, 13]
    ], 
    container = document.querySelector('#table');

this.hot = new Handsontable(container, {
    data: this.data,
    startRows: 5,
    startCols: 5,
    //minSpareCols: 1,
    //always keep at least 1 spare row at the right
    //minSpareRows: 1,
    //always keep at least 1 spare row at the bottom,
    rowHeaders: true,
    colHeaders: true,
    contextMenu: true
});
}*/
