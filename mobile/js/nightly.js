/*!
* NIGHTLY.JS
*
* Main scripts file. Contains theme functionals.
*
* Version 2.0
* Since 2.0
*/

/*
-----------------------------------------------------------------------------------------------------------*/
var m_hexcel


$(document).ready(function($) {

  "use strict";

  jQuery.mobile.ajaxEnabled = false;
  jQuery.event.special.swipe.durationThreshold = 600;
  jQuery.event.special.swipe.horizontalDistanceThreshold = 80;
  var aSearchClicked = false;

  jQuery(".sub-menu").hide();
  jQuery(".container").hide();

  if("ontouchstart" in document.documentElement){


    $('#a-sidebar').bind('touchstart touchon', function(event){
      if(aSearchClicked){
        $('#searchform').removeClass('moved');
        aSearchClicked = false;
      }
    });

    $('#a-search').bind('touchstart touchon', function(event){
      if(aSearchClicked){
        $('#searchform').removeClass('moved');
        aSearchClicked = false;
      }else{
        $('#searchform').addClass('moved');
        aSearchClicked = true;
      }
    });

  } else {


    jQuery('#header-menu-icon').bind('click', function(event){
      if(aSearchClicked){
        jQuery('#searchform').removeClass('moved');
        aSearchClicked = false;
      }
    });

    $('#a-search').bind('click', function(event){
      if(aSearchClicked){
        $('#searchform').removeClass('moved');
        aSearchClicked = false;
      }else{
        $('#searchform').addClass('moved');
        aSearchClicked = true;
      }
    });
  }

  /* Toggle for menu subcategories
    	*
    *
    * @since 3.1
    *
    */

  jQuery(".menu-item-has-children").click(function(event){
    event.preventDefault();
    jQuery(this).children(".sub-menu").toggleClass("active").toggle(350);
    return false;
  }).children(".sub-menu").children("li").click(function(event){

    window.location.href = jQuery(this).children("a").attr("href");
  });

  /* If you want to disable swipe on cetrtain elements add .disableswipe
    *
    * Example:
    * <div class="disableswipe">This is swipe disabled block</div>
    *
    * @since 1.0
    *
    */

  jQuery( document ).on( "swipeleft swiperight", '.disableswipe', function ( e ) {
    e.stopPropagation();
    e.preventDefault();
  });

  jQuery( document ).on( "swipeleft swiperight", 'input', function ( e ) {
    e.stopPropagation();
    e.preventDefault();
  });

  /* Sidebar swipe for opening / closing
    *
    * Default IDs for sidebars:
    *
    * #left-sidebar
    * #right-sidebar
    *
    * @since 1.0
    *
    */

  // Sidebar swipe opening/closing
  jQuery( document ).on( "swipeleft swiperight", function( e ) {

    if ( $.mobile.activePage.jqmData( "panel" ) !== "open" ) { // if panel isn't already open
      if ( e.type === "swipeleft"  ) { // if the swipe is from right to left
        jQuery( "#sidebar-right" ).panel( "open" ); // open right sidebar
        if(aSearchClicked){
          jQuery('#searchform').removeClass('moved');
          aSearchClicked = false;
        }
      } else if ( e.type === "swiperight" ) { // if the swipe is from left to right
        jQuery( "#sidebar" ).panel( "open" ); // open (left) sidebar with ID #sidebar
        if(aSearchClicked){
          jQuery('#searchform').removeClass('moved');
          aSearchClicked = false;
        }
      }

    }
  });

  $('#map').one("pageshow", function(){
    // code to execute on each page change
    console.log("map page init");
    init_mapa();


    var screen = $.mobile.getScreenHeight();

    var header = $(".ui-header").hasClass("ui-header-fixed") ? $(".ui-header").outerHeight()  - 1 : $(".ui-header").outerHeight();

    var footer = $(".ui-footer").hasClass("ui-footer-fixed") ? $(".ui-footer").outerHeight() - 1 : $(".ui-footer").outerHeight();

    /* content div has padding of 1em = 16px (32px top+bottom). This step
        can be skipped by subtracting 32px from content var directly. */
    var contentCurrent = $(".ui-content").outerHeight() - $(".ui-content").height();

    var content = screen - header - footer - contentCurrent;
    $("#animation").height(content-15);

    $(".ui-content").height(content);
  });
  //  init_mapa();
  m_hexcel = new HexCellSel();
  m_hexcel.onclick=function(e){
    selected_i = e.i;
    selected_j = e.j;
    console.log("i: " +e.i+ " j: " + e.j);
    streets.getSource().changed();
  };

  $(document).bind("mobileinit", function() {
    $.mobile.page.prototype.options.addBackBtn = true;
  });
  $("#street_to_grid").hide();

  $('td:nth-child(2)').hide();
  $('td:nth-child(3)').hide();
  $('td:nth-child(4)').hide();
  $('td:nth-child(5)').hide();
  $('td:nth-child(6)').hide();
  $('td:nth-child(7)').hide();
  $('td:nth-child(8)').hide();

  $("#contrib_btton").click(function(event){
    event.preventDefault();
    console.log(JSON.stringify($("#contriblist")));
    var attr = {"country": $("#contrib_country").val(),
                "city":$("#contrib_city").val(),
                "street":$("#contrib_street").val(),
                "ttowork":$("#contrib_ttowork").val(),
                "transport":$("#contrib_transport").val(),
                "traffic":$("#contrib_traffic").val(),
                "tapwater":$("#contrib_tapwater").val(),
                "riverpollution":$("#contrib_riverpollution").val(),
                "publicsafety":$("#contrib_publicsafety").val(),
                "healthservices":$("#contrib_healthservices").val(),
                "loweducation":$("#contrib_loweducation").val(),
                "higheducation":$("#contrib_higheducation").val(),
                "houseprice":$("#contrib_houseprice").val(),
                "sallary":$("#contrib_sallary").val(),
                "publictransprice":$("#contrib_publictranspprice").val(),
                "restaurantprice":$("#contrib_restaurantprice").val(),
                "fastfoodprice":$("#contrib_fasfoodprice").val()
               }
    console.log(attr);
    $.ajax({
      url: SERVER_URL_BASE + '/api/users/update',
      type: 'POST',
      data: JSON.stringify({"user":{"attr":attr}}),
      contentType: 'application/json; charset=utf-8',
      dataType: 'json',
      beforeSend : function( xhr ) {
      	xhr.setRequestHeader( 'Authorization', 'BEARER ' + SERVER_TOKEN  );
      },
      success: function(data){
        $.mobile.navigate("#tools");
      },
      error: function() {
        console.log('Error connecting to server');
        alert('Error connecting to server');
      },
      failure: function(errMsg) {
        console.log(errMsg);
        alert(errMsg);
      }
    });

    $.mobile.navigate("#tools");
  });
  $("#signup_btton").click(function(){
    var userData = {"username": $("#username_txt").val(),
                    "password": $("#password_txt").val(),
                    "email": $("#email_txt").val()};
    console.log(userData);
    $.ajax({
      url: SERVER_URL_BASE + '/api/users',
      type: 'POST',
      data: JSON.stringify(userData),
      contentType: 'application/json; charset=utf-8',
      dataType: 'json',
      //beforeSend : function( xhr ) {
      //	xhr.setRequestHeader( 'Authorization', 'BEARER ' + ApiConfig.TOKEN  );
      //},
      success: function(data){
        $.mobile.navigate("#login");
      },
      error: function() {
        console.log('Error connecting to server');
        alert('Error connecting to server');
      },
      failure: function(errMsg) {
        console.log(errMsg);
        alert(errMsg);
      }
    });
    return false;
  });
  $("#login_button").click(function(event){
    event.preventDefault();
    var user = $("#user_login_txt").val();
    var pass = $("#pass_login_txt").val();
    localStorage.user = user;
    localStorage.pass = pass;
    $.ajax({
      url: SERVER_URL_BASE + '/api/auth/basic',
      type: 'GET',
      //data: {},JSON.stringify(userData),
      contentType: 'application/json; charset=utf-8',
      dataType: 'json',
      beforeSend: function (xhr) {
        xhr.setRequestHeader ("Authorization", "Basic " + btoa(user + ":" + pass));
      },

      //beforeSend : function( xhr ) {
      //	xhr.setRequestHeader( 'Authorization', 'BEARER ' + ApiConfig.TOKEN  );
      //},
      success: function(data){
        SERVER_TOKEN=data.token;
        $.mobile.navigate("#tools");
      },
      error: function(jqXHR) {
        if (jqXHR.status == 401){
          console.log('Invalid user or password');
          alert('Invalid User or password');
        }else{
          console.log('Error connecting to server');
          alert('Error connecting to server');
        }
      },
      failure: function(errMsg) {
        console.log(errMsg);
        alert(errMsg);
      }
    });
  return false;
  });//endlogin button
  if (localStorage.user){
	var user = $("#user_login_txt").val(localStorage.user);
	var pass = $("#pass_login_txt").val(localStorage.pass);
  }else{
	  $.mobile.navigate("#firststeps");
  }
});
