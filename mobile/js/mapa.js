/*
* Copyright 2007 JORGE ARTIEDA
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/ 


var map;

var positionFeature;
var pnoa2, cicle;
var streets ,osm;
var vectorSource; //esta es la variable que coje las calles por ajax
var selected_i = -1;
var selected_j = -1;

var getlines=function(x, y,cb)  {
  var userData = {"x": x,
                  "y":y};
  console.log("coord req",userData);
  $.ajax({
    url: SERVER_URL_BASE + '/api/maps/nearestline',
    type: 'POST',
    data: JSON.stringify(userData),
    contentType: 'application/json; charset=utf-8',
    dataType: 'json',
    //beforeSend : function( xhr ) {
    //	xhr.setRequestHeader( 'Authorization', 'BEARER ' + ApiConfig.TOKEN  );
    //},
    success: function(data){
      console.log("lines", data)
      cb(data.data)
    },
    error: function() {
      console.log('Error connecting to server');
      alert('Error connecting to server');
    },
    failure: function(errMsg) {
      console.log(errMsg);
      alert(errMsg);
    }
  });
}
var clicFeatureInfo = function(pixel) {
  $.mobile.loading( "hide" ) ;
  coord = map.getCoordinateFromPixel(pixel);
  console.log(coord);
  getlines(coord[0],coord[1],function(f){
    console.log("callback"+JSON.stringify(f));
    console.log("Tipo: " +JSON.stringify( f.attr) );
        $("#street_to_grid").show(100).hide(3000);

    var row = $('<tr>')
    row.append($('<td>').text(f.name))
    .append($('<td>').text(f.attr.gdp))
    .append($('<td>').text(f.attr.camas_hosptial))
    .append($('<td>').text(f.attr.pob1k))
    .append($('<td>').text(f.attr.forest))
    .append($('<td>').text(f.attr.air))
    .append($('<td>').text(f.i))
    .append($('<td>').text(f.j))
    row.click(function(){
      $(this).addClass('selected').siblings().removeClass('selected');
      var value_i=$(this).find('td:nth-child(7)').html();
      var value_j=$(this).find('td:nth-child(8)').html();
      m_hexcel.drawPoint(value_i, value_j);
      $("#detail_table tbody").html(     '<tr><th><strong>'+$(this).find("td:nth-child(1)").html()+'</strong></th> </tr> \
<tr><th>GDP:'+$(this).find("td:nth-child(2)").html()+'</th> </tr>\
<tr><th>Hospital beds:'+$(this).find("td:nth-child(3)").html()+'</th> </tr>\
<tr><th>Population:'+$(this).find("td:nth-child(4)").html()+'</th> </tr>\
<tr><th>Forest:'+$(this).find("td:nth-child(5)").html()+'</th> </tr>\
<tr><th>Air pollution:'+$(this).find("td:nth-child(6)").html()+'</th> </tr>\
<tr><th>ij'+$(this).find("td:nth-child(7)").html()+' '+$(this).find("td:nth-child(8)").html()+'</th> </tr>'
)
    });
    $("#table2").find('tbody').append(row);
    $('#table2 td:nth-child(2)').hide();
    $('#table2 td:nth-child(3)').hide();
    $('#table2 td:nth-child(4)').hide();
    $('#table2 td:nth-child(5)').hide();
    $('#table2 td:nth-child(6)').hide();
    $('#table2 td:nth-child(7)').hide();
    $('#table2 td:nth-child(8)').hide();



  })
  
  $.mobile.loading( "hide" ) ;
};

var gotoFeature = function (name){
  
}
var displayFeatureInfo = function(pixel) {
  
};

var customStyleFunction = function(feature, resolution) {

  var strokecolor = 'rgba(0, 0, 255, 1.0)'
  if(feature.get('i')== selected_i && feature.get('j')== selected_j ) {
    strokecolor = 'rgba(255, 0, 0, 1.0)';
  }

  return [    new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: strokecolor,
      width: 2
    })
  })]
};

function init_mapa(){

  // format used to parse WFS GetFeature responses
  var geojsonFormat = new ol.format.GeoJSON();

  vectorSource = new ol.source.Vector({
    loader: function(extent, resolution, projection) {
      var url = SERVER_URL_BASE + '/cgi-bin/mapserv.fcgi?map=/var/opt/qol/server/osm_mapfile.map&service=WFS&' +
          'version=1.1.0&request=GetFeature&typename=streets&' +
          'outputFormat=geojson&' +
          //'format_options=callback:loadFeatures&' +
          'srsname=EPSG:3857&bbox=' + extent.join(',') + ',EPSG:3857';
      // use jsonp: false to prevent jQuery from adding the "callback"
      // parameter to the URL
      $.ajax({url: url, dataType: 'json', jsonp: false}).done(function( response ) {
        vectorSource.addFeatures(geojsonFormat.readFeatures(response));
      });
    },
    format: ol.format.GeoJSON,
    strategy: ol.loadingstrategy.tile(ol.tilegrid.createXYZ({
      maxZoom: 19
    }))
  });


  /**
 * JSONP WFS callback function.
 * @param {Object} response The response object.
 */
  /* window.loadFeatures = function(response) {
    console.log(response)
    vectorSource.addFeatures(geojsonFormat.readFeatures(response));
  };*/


  var cicle2 = new ol.layer.Tile({
    title:"topo",
    source: new ol.source.XYZ({
      attributions: ["thunderforest"],
      url: 'https://tile.thunderforest.com/cycle/${z}/${x}/${y}.png'
    })
  })
  var projection = ol.proj.get('EPSG:3857');
  var projectionExtent = projection.getExtent();
  var size = ol.extent.getWidth(projectionExtent) / 256;
  var resolutions = new Array(20);
  var matrixIds = new Array(20);
  for (var z = 0; z < 20; ++z) {
    // generate resolutions and matrixIds arrays for this WMTS
    resolutions[z] = size / Math.pow(2, z);
    matrixIds[z] = z;
  }
  //console.log(resolutions);
  //console.log(matrixIds);
  pnoa2=   new ol.layer.Tile({
    title:"Ortofoto",
    type: 'base',
    opacity: 0.7,
    source: new ol.source.WMTS({
      //attributions: [attribution],
      url: 'http://www.ign.es/wmts/pnoa-ma',
      layer: 'OI.OrthoimageCoverage',
      matrixSet: 'EPSG:3857',
      format: 'image/png',
      projection: projection,
      tileGrid: new ol.tilegrid.WMTS({
        origin: ol.extent.getTopLeft(projectionExtent),
        resolutions: resolutions,
        matrixIds: matrixIds
      }),
      style: 'default',
      wrapX: true
    })
  })
  cicle =new ol.layer.Tile({
    title: 'Topo',
    type: 'base',
    source: new ol.source.OSM({
      attributions: [
        new ol.Attribution({
          html: 'Tiles &copy; <a href="http://www.opencyclemap.org/">' +
          'OpenCycleMap</a>'
        }),
        ol.source.OSM.ATTRIBUTION
      ],
      url: 'http://{a-c}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png'
    })
  })

  osm = new ol.layer.Tile({ title: "osm",	type: 'base',  source: new ol.source.OSM() })

  positionFeature = new ol.Feature();
  positionFeature.setStyle(new ol.style.Style({
    image: new ol.style.Circle({
      radius: 6,
      fill: new ol.style.Fill({
        color: '#3399CC'
      }),
      stroke: new ol.style.Stroke({
        color: '#fff',
        width: 2
      })
    })
  }));

  map = new ol.Map({
    layers: [
      new ol.layer.Group({
        //title: 'Base maps',
        layers: [ pnoa2, osm
                ]
      }),new ol.layer.Group({
        //title: 'Overlays',
        layers: []}) ,

    ],
      controls: ol.control.defaults({
      attributionOptions: ({
      collapsible: true
      })
      }),
      target: 'map_container',
      view: new ol.View({
      center: [ -410000.0,
      4930000.0],
    zoom: 15,
    minZoom: 10,//minimo zoom que el usuario puede hacer
    maxZoom: 25 //maximo zoom que el usuario puede hacer
  })
});

var featuresOverlay = new ol.layer.Vector({
  map: map,
  source: new ol.source.Vector({
    features: [ positionFeature]
  })
});

/*streets = new ol.layer.Vector({
    map:map,
    source: vectorSource,
    style: customStyleFunction
  });*/
streets = new ol.layer.Image({
  map:map,
  source: new ol.source.ImageVector({
    source:vectorSource,
    style: customStyleFunction
  })
});

setPosition( "none");
//var layerSwitcher = new ol.control.LayerSwitcher({
//  tipLabel: 'Légende' // Optional label for button
//});
//map.addControl(layerSwitcher);

map.on('pointermove', function(evt) {
  if (evt.dragging) {
    return;
  }
  var pixel = map.getEventPixel(evt.originalEvent);
  displayFeatureInfo(pixel);
});

map.on('click', function(evt) {
  clicFeatureInfo(evt.pixel);
});

///este trozo de codigo marca que capa de base map se ve en cada momento
map.on('moveend', function(evt) {
  vectorSource.clear();
  console.log("moove end"+map.getView().getZoom()+": "+map.getView().getResolution() +" "+map.getView().getCenter());
  var zoom = map.getView().getZoom();
  if (zoom < 15) { //15 es el nivel de zoom a partir del cual se deja de ver la orto
    osm.setVisible (true);
    pnoa2.setVisible ( false);
  }else{
    osm.setVisible(false);
    pnoa2.setVisible( true);
  }
  if (zoom < 13){
    streets.setVisible(false);
  }else{
    streets.setVisible(true);
  }
});
$("#searchbutton").on('click', function() {
  var AdresVeld = $("#s") ;
  $.getJSON('http://nominatim.openstreetmap.org/search?format=json&q=' + AdresVeld.val(), function(data) {
    var FoundExtent = data[0].boundingbox;
    var placemark_lat = data[0].lat;
    var placemark_lon = data[0].lon;
    map.getView().setCenter(ol.proj.transform([Number(placemark_lon), Number(placemark_lat)], 'EPSG:4326', 'EPSG:3857'));
  });
  return false;
});

$( "#searchform" ).submit(function( event ) {
  var AdresVeld = $("#s") ;
  $.getJSON('http://nominatim.openstreetmap.org/search?format=json&q=' + AdresVeld.val(), function(data) {
    var FoundExtent = data[0].boundingbox;
    var placemark_lat = data[0].lat;
    var placemark_lon = data[0].lon;
    map.getView().setCenter(ol.proj.transform([Number(placemark_lon), Number(placemark_lat)], 'EPSG:4326', 'EPSG:3857'));
  });
  event.preventDefault();
  return false;
});
}

var setPosition= function ( filename){
  var coordinates = [ -596380.4964903995, 5262856.910281289]
  positionFeature.setGeometry(coordinates ?
                              new ol.geom.Point(coordinates) : null);

}
