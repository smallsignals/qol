// Load required packages
var passport = require('passport');
var BasicStrategy = require('passport-http').BasicStrategy;
var BearerStrategy = require('passport-http-bearer').Strategy;
var OAuth2Strategy = require('passport-oauth').OAuth2Strategy;

var express = require('express');
var bcrypt = require('bcrypt-nodejs');

var router = express.Router();

var base64 = require('./base64');
//localhost:3000
/*var client_id =   'f42ddb9df48f4e1c9007e3402d9df146';
var client_secret = 'ff0406535a494ec7abe8b61a277ec7f0';
var callbackURL = "http://localhost:3000/auth/callback";*/

//produccion
var client_id =   '365a2073c6434168aa3d2ff5f1f062ba';
var client_secret = '49fce34030ec413d84e9c2a8e302095b';
var callbackURL = "https://api.multifab.eu/auth/callback";

var authorizationURL = 'https://account.lab.fiware.org/oauth2/authorize';
var tokenURL = 'https://account.lab.fiware.org/oauth2/token';
//var db;
// used to serialize the user for the session
passport.serializeUser(function(user, done) {
  done(null, user.id);
});

// used to deserialize the user
passport.deserializeUser(function(id, done) {
  User.findById(id, function(err, user) {
    done(err, user);
  });
});


passport.use(new BasicStrategy(
  function(username, password, callback) {
    global.db.one("SELECT * from users where username=$1",username)
    .then(function (data) {
      bcrypt.compare(password, data.password, function(err, isMatch) {
        if (err) {
          console.log("invalid password");
          return callback(err);
        }else{
          console.log("DATA:", data);
          return callback(null, data);
        }
      });
    })
    .catch(function (error) {
      console.log("ERROR:", error);
      //return callback(err);
      return callback(null, false);
    });

  }
));

passport.use(new OAuth2Strategy({
  authorizationURL: authorizationURL,
  tokenURL: tokenURL,
  clientID: client_id,
  clientSecret: client_secret,
  callbackURL: callbackURL
},function(accessToken, refreshToken, profile, done) {
  console.log("profile:"+JSON.stringify(profile));
  /*User.findOrCreate({ username: profile.id }, function (err, user) {
    console.log("creating new user");
    return done(err, user);
    Token.findOrCreate({userId: user._id}, function(err, token){
      token.value=accessToken;
    });

  });*/
}));

passport.use(new BearerStrategy(
  function(accessToken, callback) {
    console.log(accessToken);
    db.one("SELECT * from users where token=$1",accessToken)
    .then(function (data) {
      return callback(null, data, { scope: '*' });
    })
    .catch(function (error) {
      console.log("ERROR:", error);
      //return callback(err);
      return callback(null, false);
    });

  }
));

router.get('/basic',passport.authenticate('basic'),
           function (req, res){
             var user = req.user;
             console.log(user);
             require('crypto').randomBytes(48, function(err, buffer) {
               var tokenstring = buffer.toString('hex');
               db.none("UPDATE users set token=$1 where username = $2",
                       [tokenstring,user.username])
               .then(function (data) {
                 console.log("updated token");
                 res.json({'token':tokenstring});
               })
               .catch(function (error) {
                 console.log("ERROR:", error);
                 //return callback(err);
                 res.json({'err':err});
               });
             });
           });

router.get('/user',
           passport.authenticate('bearer', { session: false }),
           function(req, res) {
             res.json(req.user);
           });

router.get('/fiware',passport.authenticate('oauth2'));

router.get('/callback',
           /*passport.authenticate('oauth2',
                                           {
                                             failureRedirect: '/login' }),
function(req, res) {
    console.log("callback");
    // Successful authentication, redirect home.
    res.redirect('/');
  }*/
           function(req, res, next) {
             console.log("callback");
             var request = require('request');
             var querystring = require('querystring');

             console.log("posting for token:"+req.query.code);
             var auth_code = base64.encode(client_id+':'+client_secret);
             var form = { grant_type:'authorization_code',
                         code: req.query.code,
                         redirect_uri: callbackURL
                        }
             var formData = querystring.stringify(form);
             console.log ("sending: " + formData);
             var contentLength = formData.length;
             request({
               headers: {
                 'Content-Length': contentLength,
                 'Content-Type': 'application/x-www-form-urlencoded',
                 'Authorization': 'Basic '+auth_code
               },
               uri: 'https://account.lab.fiware.org/oauth2/token',
               body: formData,
               method: 'POST'
             },function (error, response, body) {
               if (!error && response.statusCode == 200) {
                 console.log(body)
                 var accessToken = JSON.parse(body).access_token;

                 request({
                   uri: 'https://account.lab.fiware.org/user?access_token='+accessToken,
                   method: 'GET'},
                         function(error,response,body){
                           if (!error && response.statusCode == 200) {
                             console.log("profile:"+body);
                             var profile=JSON.parse(body);
                             /*User.findOne({ username: profile.id }, function (err, user) {
                               if (err) {
                                 res.redirect('/?token=error');
                                 //return done(err);
                               }
                               //No user was found... so create a new user with values from Facebook (all the profile. stuff)
                               if (!user) {
                                 console.log("creating new user");
                                 user = new User({
                                   username: profile.id,
                                   provider: 'fiware',
                                 });
                                 user.save(function(err) {
                                   if (err){
                                     console.log(err);
                                     res.redirect('/?token=error');
                                   }
                                   //return done(err, user);
                                 });
                               }
                               Token.findOne({userId: user._id}, function(err, token){
                                 if (err) {
                                   console.log(err);
                                   res.redirect('/?token=error');
                                   //return done(err);
                                 }
                                 //No token was found...
                                 if (!token) {
                                   console.log("creating new token");
                                   token = new Token({
                                     userId:user._id,
                                     value:accessToken
                                   });
                                   token.save(function(err) {
                                     if (err)
                                     {
                                       console.log(err);
                                       res.redirect('/?token=error');
                                     }else{
                                       res.redirect('/?token='+accessToken);
                                     }
                                     //return done(err, user);
                                   });
                                 }else{
                                   token.value=accessToken;
                                   token.save(function(err) {
                                     if (err) {
                                       console.log(err);
                                       res.redirect('/?token=error');
                                     }else{
                                       res.redirect('/?token='+accessToken);
                                     }
                                     //return done(err, user);
                                   });
                                 }
                               });

                             });
                             */
                           }else{
                             console.log("error" + error+ " status:" + response.statusCode);
                             console.log("body:" + body);
                           }
                         });

               }else{
                 console.log("error" + error+ " status:" + response.statusCode);
                 console.log("body:" + body);
               }
             }
                    );
             //res.send('hello' );*/
           }

          );
//exports.db = db;
exports.isAuthenticated = passport.authenticate(['basic','oauth2'], { session : false });
exports.isBearerAuthenticated = passport.authenticate('bearer', { session: false });
exports.r=router;
