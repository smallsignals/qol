var express = require('express');
var bcrypt = require('bcrypt-nodejs');
var passport = require('passport');

var router = express.Router();
/* users
  id integer NOT NULL DEFAULT nextval('users_id_seq'::regclass),
  username character varying(50),
  password character varying(100),
  email character varying(100),
  attr json*/
/* GET users listing. */
router.get('/', function(req, res, next) {
  req.db.many("SELECT * from users")
    .then(function (data) {
        res.json(data);
        console.log("DATA:", data.value);
    })
    .catch(function (error) {
      res.json(error);
        console.log("ERROR:", error);
    });
});
router.post('/', function(req, res, next) {
    // Password changed so we need to hash it
  console.log(req.body);
  bcrypt.genSalt(5, function(err, salt) {
    if (err) {
      res.json(err);
      return
    }
    bcrypt.hash(req.body.password, salt, null, function(err, hash) {
      if (err) return callback(err);
      var pass = hash;
      console.log(pass);
      req.db.none("INSERT INTO users (username, password,email, attr) values( $1,$2,$3,$4)",
             [req.body.username,pass,req.body.email,JSON.stringify(req.body.attr)])
      .then(function (data) {
        res.json({"res":"ok", "user":req.body.username});
        //console.log("DATA:", data.value);
      })
      .catch(function (error) {
        res.json(error);
        console.log("ERROR:", error);
      });
    });
  });
});
router.post('/update/',
            passport.authenticate('bearer', { session: false }),
                                  function(req, res, next) {
              console.log("updating user");
              console.log(req.user);
              console.log("whith attr");
              console.log(req.body);
req.db.none("update users set attr=$1 where username = $2",
             [JSON.stringify(req.body.user.attr), req.user.username])
      .then(function (data) {
        console.log("updated ok");
        res.json({"res":"ok", "user":req.body.username});
        //console.log("DATA:", data.value);
      })
      .catch(function (error) {
        res.json(error);
        console.log("ERROR:", error);
      });
 });

router.post('/delete/', function(req, res, next) {
  res.send('respond with a resource');
});
var verifyPassword = function(password, cb) {
  bcrypt.compare(password, this.password, function(err, isMatch) {
    if (err) return cb(err);
    cb(null, isMatch);
  });
};

module.exports = router;
