var express = require('express');
var passport = require('passport');

var router = express.Router();
router.post('/nearestline', function(req, res, next) {
  // Password changed so we need to hash it
  console.log(req.body);
  var q = "with index_query as ( "+
              "select "+
              "st_distance(web_geom, 'SRID=3857;POINT("+req.body.x+" "+req.body.y+")') as distance, "+
      "* from lines order by web_geom <#> 'SRID=3005;POINT("+req.body.x+" "+req.body.y+")' limit 100)"+
              "select * from index_query order by distance limit 1;"
  console.log("query", q);
  req.db.one(q)
  .then(function (data) {
    res.json({"res":"ok", "data":data});
    console.log("nearest line:", data);
  })
  .catch(function (error) {
    res.json(error);
    console.log("ERROR:", error);
  });
});

module.exports = router;
