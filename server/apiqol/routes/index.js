var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  req.db.many("SELECT attr from users")
    .then(function (data) {
        res.json(data);
        console.log("DATA:", data.value);
    })
    .catch(function (error) {
      res.json(error);
        console.log("ERROR:", error);
    });
  //res.render('index', { title: 'Express' });
});

module.exports = router;
