import psycopg2
import psycopg2.extras
import json
import os
conn = psycopg2.connect(dbname="qol", port=5432, user="web",
                            password="web", host="localhost")

for x in range(0, 40):
								
	cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
	cur.execute("select census_union.ogc_fid as uni, * from census_union where attr->'forest' is null limit 100000");
	line = cur.fetchone()
	i = 0
	while line:
		#print line
		print str(line['uni'])
						
		jsontxt = line['attr']; 
		print jsontxt; 
		if jsontxt is None:
			cur2 = conn.cursor()
			cur2.execute("""select ST_Distance(census_union.wkb_geometry, forest.geom) as dist, * \
			from lines as lines,  \
			(select * from corine where left(code_00,2)='31') as forest \
			where lines.ogc_fid=%s and ST_DWithin(forest.geom, lines.wkb_geometry, 0.1) \
			order by ST_Distance(lines.wkb_geometry, forest.geom) limit 1; \
			""",(line['uni'],));

			line2 = cur2.fetchone();
			if line2 is not None:
				print line2[0]
				cur3 = conn.cursor()
				cur3.execute("update lines set attr=%s where lines.ogc_fid=%s",('{"forest":'+str(line2[0])+'}',line['uni']))
				#conn.commit()
				print "added forest distance",line2[0]
		else:
			jsonobj=jsontxt;
			if not 'forest' in jsonobj:
				cur2 = conn.cursor()
				cur2.execute("""select ST_Distance(lines.wkb_geometry, forest.geom) as dist, * \
				from lines as lines,  \
				(select * from corine where left(code_00,2)='31') as forest \
				where lines.ogc_fid=%s and ST_DWithin(forest.geom, lines.wkb_geometry, 0.1) \
				order by ST_Distance(lines.wkb_geometry, forest.geom) limit 1; \
				""",(line['uni'],));

				line2 = cur2.fetchone();
				if line2 is not None:
					jsonobj['forest']=line2[0];
					cur3 = conn.cursor()
					cur3.execute("update lines set attr=%s where lines.ogc_fid=%s",(json.dumps(jsonobj),line['uni']))

					#cur3 = conn.cursor()
					#cur3.execute("update lines set attr=%s where lines.ogc_fid=%s",('{"forest":'+str(line2[0])+'}',line[0]))
					#conn.commit()
					print "updated forest distance:",line2[0]
			else:
				print "exist fores distance"
		
		if i == 100: 
			conn.commit()
			i=0
		i+=1
		
		line = cur.fetchone()
	print os.times()
	# Send it to PostGIS
	#curs.execute('CREATE TEMP TABLE my_lines(geom geometry, name text)')
	#curs.execute(
	#    'INSERT INTO my_lines(geom, name)'
	#    'VALUES (ST_SetSRID(%(geom)s::geometry, %(srid)s), %(name)s)',
	#    {'geom': ls.wkb_hex, 'srid': 4326, 'name': 'First Line'})

	#conn.commit()  # save data

	# Fetch the data from PostGIS
	#curs.execute('SELECT name, ST_AsText(geom) FROM my_lines')
	#curs.fetchone()  # ('First Line', 'LINESTRING Z (2.2 4.4 10.2,3.3 5.5 8.4)')
