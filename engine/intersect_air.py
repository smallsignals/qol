import psycopg2
import psycopg2.extras
import json
import os
conn = psycopg2.connect(dbname="qol", port=5432, user="web",
                            password="web", host="localhost")
for x in range(0, 20):
								
	cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
	cur.execute("select * from lines, air_pm10 where attr->'air' is null and ST_Intersects(lines.wkb_geometry, air_pm10.geom) limit 100000");
	line = cur.fetchone()
	i = 0
	while line:
		#print line
		print str(line['ogc_fid'])
						
		jsontxt = line['attr']; 
		print jsontxt; 
		if jsontxt is None:
			if line['pm10_eea'] is not None:
				cur3 = conn.cursor()
				cur3.execute("update lines set attr=%s where lines.ogc_fid=%s",('{"air":'+str(line['pm10_eea'])+'}',line['ogc_fid']))
				#conn.commit()
				print "added air"
		else:
			if line['pm10_eea'] is not None:
				jsonobj=json.loads(jsontxt);
				jsonobj['air']=line['pm10_eea'];
				cur3 = conn.cursor()
				cur3.execute("update lines set attr=%s where lines.ogc_fid=%s",(json.dumps(jsonobj),line['ogc_fid']))
				#conn.commit()
				print "added air"
		if i == 100: 
			conn.commit()
			i=0
		i+=1
			
		line = cur.fetchone()
	print os.times()
	# Send it to PostGIS
	#curs.execute('CREATE TEMP TABLE my_lines(geom geometry, name text)')
	#curs.execute(
	#    'INSERT INTO my_lines(geom, name)'
	#    'VALUES (ST_SetSRID(%(geom)s::geometry, %(srid)s), %(name)s)',
	#    {'geom': ls.wkb_hex, 'srid': 4326, 'name': 'First Line'})

	#conn.commit()  # save data

	# Fetch the data from PostGIS
	#curs.execute('SELECT name, ST_AsText(geom) FROM my_lines')
	#curs.fetchone()  # ('First Line', 'LINESTRING Z (2.2 4.4 10.2,3.3 5.5 8.4)')
