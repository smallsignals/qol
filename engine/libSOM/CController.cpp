#include "CController.h"
#include <stdio.h>
#include <stdlib.h>
#include <libpq-fe.h>

static void
exit_nicely(PGconn *conn)
{
    PQfinish(conn);
    exit(1);
}

double weights[5]={0.01, 1000, 0.001, 1 ,1 };

//---------------------------- Render ------------------------------------
//
//------------------------------------------------------------------------
void CController::Render()
{
  m_pSOM->Render(stdout);
}
void CController::Save(FILE *f)
{
  m_pSOM->Render(f);
}
void CController::Read(FILE *f)
{
  m_pSOM->Read(f);
}

void CController::Clasify(){
    const char *conninfo;
    PGconn     *conn;
    PGresult   *res;
    int         nFields;
    int         i;
    int 		j;
    printf("Clasify\n");
    /*
     * If the user supplies a parameter on the command line, use it as the
     * conninfo string; otherwise default to setting dbname=postgres and using
     * environment variables or defaults for all other connection parameters.
     */
    conninfo = "dbname = qol user = web password = web host=localhost port = 5432";

    /* Make a connection to the database */
    conn = PQconnectdb(conninfo);

    /* Check to see that the backend connection was successfully made */
    if (PQstatus(conn) != CONNECTION_OK)
    {
        fprintf(stderr, "Connection to database failed: %s",
                PQerrorMessage(conn));
        exit_nicely(conn);
    }

    res = PQexec(conn, "select ogc_fid, attr->'pob1k',attr->'forest',"
    		"attr->'air',attr->'camas_hosptial' ,attr->'gdp'  from lines where "
    		"attr->'pob1k' is not null and attr->'forest' is not null "
    		"and attr->'gdp' is not null "
    		"and attr->'air' is not null and attr->'camas_hosptial' is not null "
    		"and i is null limit 1000");
//    		"and random() < 0.01 limit 1000");
    if (PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        fprintf(stderr, "select * from lines where attr->'pob' is not null FAILED: %s", PQerrorMessage(conn));
        PQclear(res);
        exit_nicely(conn);
    }

    /* first, print out the attribute names */
    nFields = PQnfields(res) - 1 ;
    /*for (i = 0; i < nFields; i++)
        printf("%-15s", PQfname(res, i));
    printf("\n\n");
	*/
    PGresult   *res2;

    /* Start a transaction block */
	res2 = PQexec(conn, "BEGIN");
	if (PQresultStatus(res2) != PGRES_COMMAND_OK)
	{
	   printf("BEGIN command failed: %s", PQerrorMessage(conn));
	   PQclear(res2);
	   exit_nicely(conn);
	}
	PQclear(res2);
    printf("nrows %d \n",PQntuples(res));
    if(PQntuples(res)==0){
	printf("no mor rows!");
	exit (0);
    }
    /* next, print out the rows */
    for (i = 0; i < PQntuples(res); i++)
    {
    	vector<double> vin;

        for (j = 0; j < nFields; j++){
        	//first field is fid so j+1
            printf("%5.5f ", atof(PQgetvalue(res, i, j+1)));
        	vin.push_back(atof(PQgetvalue(res, i, j+1))*weights[j]);
    	}
        vector<int> vout(2);
		m_pSOM->NearestCell(vin, vout);
		printf("--> %d %d", vout[0],vout[1] );
        printf("\n");
        char query[300];
        sprintf(query , "update lines set i=%d, j=%d where lines.ogc_fid=%d",
        		vout[0],vout[1],atoi(PQgetvalue(res, i, 0)));//first fiel is fid

        res2 = PQexec(conn,query);
		if (PQresultStatus(res2) != PGRES_COMMAND_OK)
		{
		   printf("UPDATE command failed: %s", PQerrorMessage(conn));
		   PQclear(res2);
		   exit_nicely(conn);
		}

        PQclear(res2);
    }
    /* end the transaction */
	res2 = PQexec(conn, "END");
	PQclear(res2);
    PQclear(res);

    /* close the connection to the database and cleanup */
    PQfinish(conn);

}
//--------------------------- Train --------------------------------------
//
// trains the network given a std::vector of input vectors
//------------------------------------------------------------------------
bool CController::Train()
{
  if (!m_pSOM->FinishedTraining())
  {
    if (!m_pSOM->Epoch(m_TrainingSet))
    {
      return false;
    }
  }

  return true;
}

//-------------------------- CreateDataSet -------------------------------
//
//------------------------------------------------------------------------
void CController::CreateDataSet()
{
#ifdef POSTGRES_DATASET
    const char *conninfo;
    PGconn     *conn;
    PGresult   *res;
    int         i,
                j;

    /*
     * If the user supplies a parameter on the command line, use it as the
     * conninfo string; otherwise default to setting dbname=postgres and using
     * environment variables or defaults for all other connection parameters.
     */
    conninfo = "dbname = qol user = web password = web host = localhost port = 5432";

    /* Make a connection to the database */
    conn = PQconnectdb(conninfo);

    /* Check to see that the backend connection was successfully made */
    if (PQstatus(conn) != CONNECTION_OK)
    {
        fprintf(stderr, "Connection to database failed: %s",
                PQerrorMessage(conn));
        exit_nicely(conn);
    }

    /*
     * Our test case here involves using a cursor, for which we must be inside
     * a transaction block.  We could do the whole thing with a single
     * PQexec() of "select * from pg_database", but that's too trivial to make
     * a good example.
     */

    /*
     * Fetch rows from pg_database, the system catalog of databases
     */
    res = PQexec(conn, "select attr->'pob1k',attr->'forest',"
    		"attr->'air',attr->'camas_hosptial',attr->'gdp'  from lines where "
    		"attr->'pob1k' is not null and attr->'forest' is not null "
    		"and attr->'air' is not null and attr->'camas_hosptial' is not null "
    		"and attr->'gdp' is not null "
    		"and random() < 0.01 limit 1000");
    if (PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        fprintf(stderr, "select * from lines where attr->'pob' is not null FAILED: %s", PQerrorMessage(conn));
        PQclear(res);
        exit_nicely(conn);
    }

    /* first, print out the attribute names */
    nFields = PQnfields(res);

    for (i = 0; i < nFields; i++)
        printf("%-15s", PQfname(res, i));
    printf("\n\n");
    printf("res dataset: %d\n",PQntuples(res));
    /* next, print out the rows */
    for (i = 0; i < PQntuples(res); i++)
    {
    	vector<double> purple;

        for (j = 0; j < nFields; j++){
            printf("%-15s", PQgetvalue(res, i, j));
        	purple.push_back(atof(PQgetvalue(res, i, j))*weights[j]);
    	}
        printf("\n");
		m_TrainingSet.push_back(purple);

    }

    PQclear(res);

    /* close the connection to the database and cleanup */
    PQfinish(conn);



#else

#ifndef RANDOM_TRAINING_SETS
  
  //create a data set
  vector<double> red, green, blue, yellow, orange, purple, dk_green, dk_blue;

  red.push_back(1);
  red.push_back(0);
  red.push_back(0);

  green.push_back(0);
  green.push_back(1);
  green.push_back(0);

  dk_green.push_back(0);
  dk_green.push_back(0.5);
  dk_green.push_back(0.25);

  blue.push_back(0);
  blue.push_back(0);
  blue.push_back(1);

  dk_blue.push_back(0);
  dk_blue.push_back(0);
  dk_blue.push_back(0.5);

  yellow.push_back(1);
  yellow.push_back(1);
  yellow.push_back(0.2);

  orange.push_back(1);
  orange.push_back(0.4);
  orange.push_back(0.25);

  purple.push_back(1);
  purple.push_back(0);
  purple.push_back(1);
  /*
  m_TrainingSet.push_back(red);
  m_TrainingSet.push_back(green);
  m_TrainingSet.push_back(blue);
  m_TrainingSet.push_back(yellow);
  m_TrainingSet.push_back(orange);
  m_TrainingSet.push_back(purple);
  m_TrainingSet.push_back(dk_green);
  m_TrainingSet.push_back(dk_blue);*/
  
  double dataset [150][4]={
	  0.2222222222	,	0.625	,	0.0677966102	,	0.0416666667	,
0.1666666667	,	0.4166666667	,	0.0677966102	,	0.0416666667	,
0.1111111111	,	0.5	,	0.0508474576	,	0.0416666667	,
0.0833333333	,	0.4583333333	,	0.0847457627	,	0.0416666667	,
0.1944444444	,	0.6666666667	,	0.0677966102	,	0.0416666667	,
0.3055555556	,	0.7916666667	,	0.1186440678	,	0.125	,
0.0833333333	,	0.5833333333	,	0.0677966102	,	0.0833333333	,
0.1944444444	,	0.5833333333	,	0.0847457627	,	0.0416666667	,
0.0277777778	,	0.375	,	0.0677966102	,	0.0416666667	,
0.1666666667	,	0.4583333333	,	0.0847457627	,	0	,
0.3055555556	,	0.7083333333	,	0.0847457627	,	0.0416666667	,
0.1388888889	,	0.5833333333	,	0.1016949153	,	0.0416666667	,
0.1388888889	,	0.4166666667	,	0.0677966102	,	0	,
0	,	0.4166666667	,	0.0169491525	,	0	,
0.4166666667	,	0.8333333333	,	0.0338983051	,	0.0416666667	,
0.3888888889	,	1	,	0.0847457627	,	0.125	,
0.3055555556	,	0.7916666667	,	0.0508474576	,	0.125	,
0.2222222222	,	0.625	,	0.0677966102	,	0.0833333333	,
0.3888888889	,	0.75	,	0.1186440678	,	0.0833333333	,
0.2222222222	,	0.75	,	0.0847457627	,	0.0833333333	,
0.3055555556	,	0.5833333333	,	0.1186440678	,	0.0416666667	,
0.2222222222	,	0.7083333333	,	0.0847457627	,	0.125	,
0.0833333333	,	0.6666666667	,	0	,	0.0416666667	,
0.2222222222	,	0.5416666667	,	0.1186440678	,	0.1666666667	,
0.1388888889	,	0.5833333333	,	0.1525423729	,	0.0416666667	,
0.1944444444	,	0.4166666667	,	0.1016949153	,	0.0416666667	,
0.1944444444	,	0.5833333333	,	0.1016949153	,	0.125	,
0.25	,	0.625	,	0.0847457627	,	0.0416666667	,
0.25	,	0.5833333333	,	0.0677966102	,	0.0416666667	,
0.1111111111	,	0.5	,	0.1016949153	,	0.0416666667	,
0.1388888889	,	0.4583333333	,	0.1016949153	,	0.0416666667	,
0.3055555556	,	0.5833333333	,	0.0847457627	,	0.125	,
0.25	,	0.875	,	0.0847457627	,	0	,
0.3333333333	,	0.9166666667	,	0.0677966102	,	0.0416666667	,
0.1666666667	,	0.4583333333	,	0.0847457627	,	0	,
0.1944444444	,	0.5	,	0.0338983051	,	0.0416666667	,
0.3333333333	,	0.625	,	0.0508474576	,	0.0416666667	,
0.1666666667	,	0.4583333333	,	0.0847457627	,	0	,
0.0277777778	,	0.4166666667	,	0.0508474576	,	0.0416666667	,
0.2222222222	,	0.5833333333	,	0.0847457627	,	0.0416666667	,
0.1944444444	,	0.625	,	0.0508474576	,	0.0833333333	,
0.0555555556	,	0.125	,	0.0508474576	,	0.0833333333	,
0.0277777778	,	0.5	,	0.0508474576	,	0.0416666667	,
0.1944444444	,	0.625	,	0.1016949153	,	0.2083333333	,
0.2222222222	,	0.75	,	0.1525423729	,	0.125	,
0.1388888889	,	0.4166666667	,	0.0677966102	,	0.0833333333	,
0.2222222222	,	0.75	,	0.1016949153	,	0.0416666667	,
0.0833333333	,	0.5	,	0.0677966102	,	0.0416666667	,
0.2777777778	,	0.7083333333	,	0.0847457627	,	0.0416666667	,
0.1944444444	,	0.5416666667	,	0.0677966102	,	0.0416666667	,
0.75	,	0.5	,	0.6271186441	,	0.5416666667	,
0.5833333333	,	0.5	,	0.593220339	,	0.5833333333	,
0.7222222222	,	0.4583333333	,	0.6610169492	,	0.5833333333	,
0.3333333333	,	0.125	,	0.5084745763	,	0.5	,
0.6111111111	,	0.3333333333	,	0.6101694915	,	0.5833333333	,
0.3888888889	,	0.3333333333	,	0.593220339	,	0.5	,
0.5555555556	,	0.5416666667	,	0.6271186441	,	0.625	,
0.1666666667	,	0.1666666667	,	0.3898305085	,	0.375	,
0.6388888889	,	0.375	,	0.6101694915	,	0.5	,
0.25	,	0.2916666667	,	0.4915254237	,	0.5416666667	,
0.1944444444	,	0	,	0.4237288136	,	0.375	,
0.4444444444	,	0.4166666667	,	0.5423728814	,	0.5833333333	,
0.4722222222	,	0.0833333333	,	0.5084745763	,	0.375	,
0.5	,	0.375	,	0.6271186441	,	0.5416666667	,
0.3611111111	,	0.375	,	0.4406779661	,	0.5	,
0.6666666667	,	0.4583333333	,	0.5762711864	,	0.5416666667	,
0.3611111111	,	0.4166666667	,	0.593220339	,	0.5833333333	,
0.4166666667	,	0.2916666667	,	0.5254237288	,	0.375	,
0.5277777778	,	0.0833333333	,	0.593220339	,	0.5833333333	,
0.3611111111	,	0.2083333333	,	0.4915254237	,	0.4166666667	,
0.4444444444	,	0.5	,	0.6440677966	,	0.7083333333	,
0.5	,	0.3333333333	,	0.5084745763	,	0.5	,
0.5555555556	,	0.2083333333	,	0.6610169492	,	0.5833333333	,
0.5	,	0.3333333333	,	0.6271186441	,	0.4583333333	,
0.5833333333	,	0.375	,	0.5593220339	,	0.5	,
0.6388888889	,	0.4166666667	,	0.5762711864	,	0.5416666667	,
0.6944444444	,	0.3333333333	,	0.6440677966	,	0.5416666667	,
0.6666666667	,	0.4166666667	,	0.6779661017	,	0.6666666667	,
0.4722222222	,	0.375	,	0.593220339	,	0.5833333333	,
0.3888888889	,	0.25	,	0.4237288136	,	0.375	,
0.3333333333	,	0.1666666667	,	0.4745762712	,	0.4166666667	,
0.3333333333	,	0.1666666667	,	0.4576271186	,	0.375	,
0.4166666667	,	0.2916666667	,	0.4915254237	,	0.4583333333	,
0.4722222222	,	0.2916666667	,	0.6949152542	,	0.625	,
0.3055555556	,	0.4166666667	,	0.593220339	,	0.5833333333	,
0.4722222222	,	0.5833333333	,	0.593220339	,	0.625	,
0.6666666667	,	0.4583333333	,	0.6271186441	,	0.5833333333	,
0.5555555556	,	0.125	,	0.5762711864	,	0.5	,
0.3611111111	,	0.4166666667	,	0.5254237288	,	0.5	,
0.3333333333	,	0.2083333333	,	0.5084745763	,	0.5	,
0.3333333333	,	0.25	,	0.5762711864	,	0.4583333333	,
0.5	,	0.4166666667	,	0.6101694915	,	0.5416666667	,
0.4166666667	,	0.25	,	0.5084745763	,	0.4583333333	,
0.1944444444	,	0.125	,	0.3898305085	,	0.375	,
0.3611111111	,	0.2916666667	,	0.5423728814	,	0.5	,
0.3888888889	,	0.4166666667	,	0.5423728814	,	0.4583333333	,
0.3888888889	,	0.375	,	0.5423728814	,	0.5	,
0.5277777778	,	0.375	,	0.5593220339	,	0.5	,
0.2222222222	,	0.2083333333	,	0.3389830508	,	0.4166666667	,
0.3888888889	,	0.3333333333	,	0.5254237288	,	0.5	,
0.5555555556	,	0.5416666667	,	0.8474576271	,	1	,
0.4166666667	,	0.2916666667	,	0.6949152542	,	0.75	,
0.7777777778	,	0.4166666667	,	0.8305084746	,	0.8333333333	,
0.5555555556	,	0.375	,	0.7796610169	,	0.7083333333	,
0.6111111111	,	0.4166666667	,	0.813559322	,	0.875	,
0.9166666667	,	0.4166666667	,	0.9491525424	,	0.8333333333	,
0.1666666667	,	0.2083333333	,	0.593220339	,	0.6666666667	,
0.8333333333	,	0.375	,	0.8983050847	,	0.7083333333	,
0.6666666667	,	0.2083333333	,	0.813559322	,	0.7083333333	,
0.8055555556	,	0.6666666667	,	0.8644067797	,	1	,
0.6111111111	,	0.5	,	0.6949152542	,	0.7916666667	,
0.5833333333	,	0.2916666667	,	0.7288135593	,	0.75	,
0.6944444444	,	0.4166666667	,	0.7627118644	,	0.8333333333	,
0.3888888889	,	0.2083333333	,	0.6779661017	,	0.7916666667	,
0.4166666667	,	0.3333333333	,	0.6949152542	,	0.9583333333	,
0.5833333333	,	0.5	,	0.7288135593	,	0.9166666667	,
0.6111111111	,	0.4166666667	,	0.7627118644	,	0.7083333333	,
0.9444444444	,	0.75	,	0.9661016949	,	0.875	,
0.9444444444	,	0.25	,	1	,	0.9166666667	,
0.4722222222	,	0.0833333333	,	0.6779661017	,	0.5833333333	,
0.7222222222	,	0.5	,	0.7966101695	,	0.9166666667	,
0.3611111111	,	0.3333333333	,	0.6610169492	,	0.7916666667	,
0.9444444444	,	0.3333333333	,	0.9661016949	,	0.7916666667	,
0.5555555556	,	0.2916666667	,	0.6610169492	,	0.7083333333	,
0.6666666667	,	0.5416666667	,	0.7966101695	,	0.8333333333	,
0.8055555556	,	0.5	,	0.8474576271	,	0.7083333333	,
0.5277777778	,	0.3333333333	,	0.6440677966	,	0.7083333333	,
0.5	,	0.4166666667	,	0.6610169492	,	0.7083333333	,
0.5833333333	,	0.3333333333	,	0.7796610169	,	0.8333333333	,
0.8055555556	,	0.4166666667	,	0.813559322	,	0.625	,
0.8611111111	,	0.3333333333	,	0.8644067797	,	0.75	,
1	,	0.75	,	0.9152542373	,	0.7916666667	,
0.5833333333	,	0.3333333333	,	0.7796610169	,	0.875	,
0.5555555556	,	0.3333333333	,	0.6949152542	,	0.5833333333	,
0.5	,	0.25	,	0.7796610169	,	0.5416666667	,
0.9444444444	,	0.4166666667	,	0.8644067797	,	0.9166666667	,
0.5555555556	,	0.5833333333	,	0.7796610169	,	0.9583333333	,
0.5833333333	,	0.4583333333	,	0.7627118644	,	0.7083333333	,
0.4722222222	,	0.4166666667	,	0.6440677966	,	0.7083333333	,
0.7222222222	,	0.4583333333	,	0.7457627119	,	0.8333333333	,
0.6666666667	,	0.4583333333	,	0.7796610169	,	0.9583333333	,
0.7222222222	,	0.4583333333	,	0.6949152542	,	0.9166666667	,
0.4166666667	,	0.2916666667	,	0.6949152542	,	0.75	,
0.6944444444	,	0.5	,	0.8305084746	,	0.9166666667	,
0.6666666667	,	0.5416666667	,	0.7966101695	,	1	,
0.6666666667	,	0.4166666667	,	0.7118644068	,	0.9166666667	,
0.5555555556	,	0.2083333333	,	0.6779661017	,	0.75	,
0.6111111111	,	0.4166666667	,	0.7118644068	,	0.7916666667	,
0.5277777778	,	0.5833333333	,	0.7457627119	,	0.9166666667	,
0.4444444444	,	0.4166666667	,	0.6949152542	,	0.7083333333	
};
for (int i = 0; i< 150; i++){
	m_TrainingSet.push_back(std::vector<double> (&dataset[i][0], &dataset[i][0]+4));
}
#else

  //choose a random number of training sets
  int NumSets = RandInt(constMinNumTrainingSets, constMaxNumTrainingSets);

  for (int s=0; s<NumSets; ++s)
  {

    vector<double> set;

    set.push_back(RandFloat());
    set.push_back(RandFloat());
    set.push_back(RandFloat());

    m_TrainingSet.push_back(set);
  }

#endif
#endif
}
