//============================================================================
// Name        : libSOM.cpp
// Author      : Jorge Artieda
// Version     :
// Copyright   : All rights reserved
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "CController.h"

int main() {
	cout << "!!!SOM Engine!!!" << endl; // prints !!!Hello World!!!

	CController* pcontroller = new CController(5, 9, 5, 9,1000);
	/*FILE* ff = fopen("clases.txt","r");
	pcontroller->Read(ff);
	fclose(ff);*/
	cout<<"Train"<<endl;
	pcontroller->Train();
	cout<<"Render"<<endl;
	pcontroller->Render();
	for (int i = 0; i< 3000; i++){
		cout<<"Clasify: "<<i<<endl;
		pcontroller->Clasify();
	}
	FILE* f;
	f = fopen ("clases.txt","w");
	pcontroller->Save(f);
	fclose(f);
	return 0;
}
