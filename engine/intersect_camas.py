import psycopg2
import psycopg2.extras
import json
import os
conn = psycopg2.connect(dbname="postgis_22_sample", port=5433, user="postgres",
                            password="3de22ee3", host="localhost")

for x in range(0, 30):
	#table = "poblacion"		
	#attrname = "pob"
	#field = "t_total"
	table = "camas_hospital"		
	attrname = "camas_hosptial"
	field = "camas_ho_1"
	cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
	cur.execute("select lines.ogc_fid as uni, * from lines, "+table+"  where attr->'"+attrname+"' is null \
				and ST_Intersects(lines.wkb_geometry, "+table+".wkb_geometry) limit 100000");
	line = cur.fetchone()
	i = 0
	while line:
		#print line
		print "fid: " + str(line['uni'])+" " + str(line['osm_id'])
						
		jsontxt = line['attr']; 
		print jsontxt; 
		camas = line[field]
		if camas < 0:
			camas = line['camas_ho_2']
		if camas < 0:
			camas = line['camas_ho_3']
		if camas < 0:
			camas = line['camas_ho_4']
		if camas < 0:
			camas = line['camas_ho_5']
		if camas < 0:
			camas = line['camas_ho_6']
		camas = float(camas);
		if jsontxt is None:
			if line[field] is not None:
				cur3 = conn.cursor()
				cur3.execute("update lines set attr=%s where lines.ogc_fid=%s",('{"'+attrname+'":'+str(camas)+'}',line['uni']))
				#conn.commit()
				print "added "+field+str(camas)
		else:
			jsonobj=jsontxt;
			jsonobj[attrname]=camas;
			cur3 = conn.cursor()
			cur3.execute("update lines set attr=%s where lines.ogc_fid=%s",(json.dumps(jsonobj),line['uni']))
			#conn.commit()
			print "updated "+field+str(camas)
		print i
		if i == 100: 
			print "commit"
			conn.commit()
			i=0
		i+=1
		line = cur.fetchone()
	print os.times()
	# Send it to PostGIS
	#curs.execute('CREATE TEMP TABLE my_lines(geom geometry, name text)')
	#curs.execute(
	#    'INSERT INTO my_lines(geom, name)'
	#    'VALUES (ST_SetSRID(%(geom)s::geometry, %(srid)s), %(name)s)',
	#    {'geom': ls.wkb_hex, 'srid': 4326, 'name': 'First Line'})

	#conn.commit()  # save data

	# Fetch the data from PostGIS
	#curs.execute('SELECT name, ST_AsText(geom) FROM my_lines')
	#curs.fetchone()  # ('First Line', 'LINESTRING Z (2.2 4.4 10.2,3.3 5.5 8.4)')