import psycopg2
import psycopg2.extras
import json
import os
conn = psycopg2.connect(dbname="qol", port=5432, user="web",
                            password="web", host="localhost")
for x in range(0, 500):

	table = "poblacion_1km"		
	attrname = "pob1k"
	field = "tot_p"

	cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
	cur.execute("select lines.ogc_fid as uni, * from lines, "+table+"  where attr->'pob1k' is null and ST_Intersects(lines.wkb_geometry, "+table+".wkb_geometry) limit 10000");
	line = cur.fetchone()
	i = 0
	while line:
		#print line
		print str(line['uni'])
						
		jsontxt = line['attr']; 
		print jsontxt; 
		if jsontxt is None:
			if line[field] is not None:
				cur3 = conn.cursor()
				cur3.execute("update lines set attr=%s where lines.ogc_fid=%s",('{"'+attrname+'":'+str(line[field])+'}',line['uni']))
				#conn.commit()
				print "added pop"
		else:
		   try:
			jsonobj=json.loads(jsontxt);
			jsonobj[attrname]=float(line[field]);
			cur3 = conn.cursor()
			cur3.execute("update lines set attr=%s where lines.ogc_fid=%s",(json.dumps(jsonobj),line['uni']))
			#conn.commit()
			print "added "+field+" "+str(float(line[field]))
                   except TypeError:
			print "Opps not a float "+str( line[field]);

		if i == 100: 
			conn.commit()
			i=0
		i+=1
		line = cur.fetchone()
	print os.times()
	# Send it to PostGIS
	#curs.execute('CREATE TEMP TABLE my_lines(geom geometry, name text)')
	#curs.execute(
	#    'INSERT INTO my_lines(geom, name)'
	#    'VALUES (ST_SetSRID(%(geom)s::geometry, %(srid)s), %(name)s)',
	#    {'geom': ls.wkb_hex, 'srid': 4326, 'name': 'First Line'})

	#conn.commit()  # save data

	# Fetch the data from PostGIS
	#curs.execute('SELECT name, ST_AsText(geom) FROM my_lines')
	#curs.fetchone()  # ('First Line', 'LINESTRING Z (2.2 4.4 10.2,3.3 5.5 8.4)')
